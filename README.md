# requests

## 文件结构

### config.go

定义了返回的结构体，目前包含了响应包体、请求报文、状态码。该文件也定义了对传入参数(header,cookie,param)的处理。

## usage

### 发送请求

```go
res := requests.Get("http://xxx.com")
```

### 传参

如果需要定义**header**等，则可以使用以下方式：

```go
cookie := requests.Cookie{
	"session": "1",
}
header := requests.Header{
	"User-Agent": "xxx",
}
param := requests.Param{
	"name": "Cristrik010",
	"age":  18,
}
res := requests.Get("http://xxx.com", cookie, param)
```

需要注意的是param是`map[string]interface{}`类型

### 设置请求超时时间

在定义请求时可以直接传入超时时间：

```go
res := requests.Get("http://xxx.com", time.Second*3)
```

### 设置代理

在发送请求时可以，传入已经定义好的`requests.Proxy("xxx")`类型。目前支持**socks**和**http**协议的代理。样例：

```go
url := "http://xxx.com"
res := requests.Get(url, requests.Proxy("xxx"))
```

## Content-Type

当发送**POST**请求的时候会有各种各样的请求体，该库提供了两种**Content-Type**：json和表单。

#### json

有时发送post请求需要以json格式发送，这个时候可以在传参时传入一个bool值表示参数是json。例如：

```go
param := requests.Param{
	"name": "Cristrik010",
	"age":  18,
}
res := requests.Get("http://xxx.com", param, true)
```

此时就会自动将`requests.Param`转换为**json**字符串进行传参。

#### 表单

当传参时不传入**bool**值则表示以表单形式传参。例如：

```go
param := requests.Param{
	"name": "Cristrik010",
	"age":  18,
}
res := requests.Get("http://xxx.com", param)
```

#### 其它

有时**POST**发送的请求体不一定是**json**或表单数据，这个时候也支持自定义**Body**：

```go
body := requests.Body("Hello, World!")
res := requests.Post("http://xxx.com", body)
```

这个时候默认是不会在请求头添加**Content-Type**字段的，如有需要可以自行定义header。

## 返回值

请求成功后会返回一个自定义的结构体，现在仅有三个自定义的属性：

```go
Status   string // 此次请求状态码
Text     string // 此次请求响应包体
HttpText string // 当前请求的报文
```

该结构体还继承了**http.Request**。

## 错误

当请求出现错误的时候会返回一个空指针即nil，因此可以通过这个特征来判断是否出现错误。